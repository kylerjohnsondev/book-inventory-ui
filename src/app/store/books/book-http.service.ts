import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Book } from '../../global-models/book';
import { HttpClient } from '@angular/common/http';
import { AddBookDto } from '../../global-models';

@Injectable({
  providedIn: 'root'
})
export class BookHttpService {

  private readonly url = 'http://localhost:3000';

  constructor(private http: HttpClient) { }

  getAllBooks(): Observable<Book[]> {
    return this.http.get<Book[]>(`${this.url}/books`);
  }

  updateBook(book: Book): Observable<Book> {
    return this.http.patch<Book>(`${this.url}/books`, book);
  }

  addNewBook(bookToAdd: AddBookDto): Observable<Book> {
    return this.http.post<Book>(`${this.url}/books`, bookToAdd);
  }

  deleteBook(id: string): Observable<Book> {
    return this.http.delete<Book>(`${this.url}/books/${id}`);
  }

}
