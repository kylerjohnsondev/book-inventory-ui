import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Book, AddBookDto } from './../../global-models';
import { map } from 'rxjs/operators';
import { BookHttpService } from './book-http.service';

@Injectable({
  providedIn: 'root'
})
export class BooksStoreService {

  private booksSubject$ = new BehaviorSubject<Book[]>(null);
  readonly books$: Observable<Book[]> = this.booksSubject$.asObservable();

  constructor(private bookHttp: BookHttpService) { }

  setBooks(): void {
      this.bookHttp
        .getAllBooks()
        .subscribe(books => this.booksSubject$.next(books));
  }

  getBookById(id: string): Observable<Book> {
    if(!this.booksSubject$.value) this.setBooks();
    return this.books$.pipe(
      map(books => {
        return books.find(book => book._id === id);
      })
    )
  }

  updateBook(book: Book): void {
    this.bookHttp.updateBook(book)
      .subscribe(updatedBook => {
        const books = [...this.booksSubject$.value];
        const bookIndex = books.findIndex(bk => bk._id === updatedBook._id);
        books.splice(bookIndex, 1, updatedBook);
        this.booksSubject$.next(books);
      });
  }

  addNewBook(bookToAdd: AddBookDto): void {
    this.bookHttp
      .addNewBook(bookToAdd)
      .subscribe(bookAdded => {
        const currentBooks = [...this.booksSubject$.value, bookAdded];
        this.booksSubject$.next(currentBooks);
      });
  }

  deleteBook(id: string) {
    this.bookHttp
      .deleteBook(id)
      .subscribe(deletedBook => {
        const booksWithoutDeletedBook = this.booksSubject$.value
          .filter(book => book._id !== deletedBook._id);
        this.booksSubject$.next(booksWithoutDeletedBook);
      });
  }

}
