import { TestBed } from '@angular/core/testing';

import { GenreStoreService } from './genre-store.service';

describe('GenreStoreService', () => {
  let service: GenreStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GenreStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
