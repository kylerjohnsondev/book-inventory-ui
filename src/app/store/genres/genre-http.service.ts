import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Genre } from 'src/app/global-models';

@Injectable({
  providedIn: 'root'
})
export class GenreHttpService {

  private readonly url = 'http://localhost:3000';

  constructor(private http: HttpClient) { }

  getAllGenres(): Observable<Genre[]> {
    return this.http.get<Genre[]>(`${this.url}/genres`);
  }
}
