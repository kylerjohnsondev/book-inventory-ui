import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Genre } from 'src/app/global-models';
import { GenreHttpService } from './genre-http.service';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GenreStoreService {

  private genresSubject$ = new BehaviorSubject<Genre[]>([]);
  readonly genres$: Observable<Genre[]> = this.genresSubject$.asObservable();

  readonly genreNames$: Observable<string[]> = this.genres$.pipe(
    map(genres => genres.map(genre => genre.name))
  );

  constructor(private genreHttp: GenreHttpService) { }

  setGenres(): void {
    this.genreHttp
      .getAllGenres()
      .pipe(tap(genres => console.log(genres)))
      .subscribe(genres => this.genresSubject$.next(genres));
  }
}
