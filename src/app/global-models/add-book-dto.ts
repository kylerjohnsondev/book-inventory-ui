export class AddBookDto {
    constructor(
      public title: string,
      public author: string,
      public genre: string,
      public price: number
    ) {}
  }