export class Book {
  constructor(
    public _id: string,
    public title: string,
    public author: string,
    public genre: string,
    public price: number
  ) {}
}
