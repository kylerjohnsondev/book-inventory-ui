import { Component, OnInit } from '@angular/core';
import { BooksStoreService } from '../../store';
import { GenreStoreService } from 'src/app/store/genres/genre-store.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent implements OnInit {

  constructor(
    public bookStore: BooksStoreService,
    private genreStore: GenreStoreService
  ) {}

  ngOnInit() {
    // set initial state
    this.bookStore.setBooks();
    this.genreStore.setGenres();
  }

}
