import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SubComponentDirective } from '../../../shared/directives/sub-component.directive';
import { Observable } from 'rxjs';
import { map, withLatestFrom, filter } from 'rxjs/operators';
import { BooksStoreService, GenreStoreService } from './../../../store';
import { AddBookDto } from '../../../global-models';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.scss'],
})
export class AddBookComponent extends SubComponentDirective implements OnInit {

  filteredOptions$: Observable<string[]>;
  addBookForm: FormGroup;

  constructor(
    public formBuilder: FormBuilder,
    private bookStore: BooksStoreService,
    private genreStore: GenreStoreService
  ) { super(); }

  ngOnInit() {
    this.buildFormGroup();

    this.filteredOptions$ = this.addBookForm.controls.genre.valueChanges.pipe(
      filter(genreInput => !!genreInput),
      withLatestFrom(this.genreStore.genreNames$),
      map(([genre, genres]) => ({ genre, genres })),
      map(filterData => this.filterGenres(filterData.genre, filterData.genres))
    );
  }

  connect() {
    return this.bookStore.books$;
  }

  buildFormGroup(): void {
    this.addBookForm = this.formBuilder.group({
      title: ['', Validators.required],
      author: ['', Validators.required],
      genre: ['', Validators.required],
      price: ['', Validators.required]
    });
  }

  addBook(): void {
    if (this.addBookForm.invalid) {
      console.log('Add Book Form Invalid');
      return;
    }

    const bookToAdd = new AddBookDto(
      this.addBookForm.value.title,
      this.addBookForm.value.author,
      this.addBookForm.value.genre,
      +this.addBookForm.value.price
    );

    this.bookStore.addNewBook(bookToAdd);
    this.addBookForm.reset();
  }

  private filterGenres(genre: string, genres: string[]): string[] {
    const filterValue = genre.toLowerCase();
    return genres.filter(option =>
        option.toLowerCase()
        .includes(filterValue)
    );
  }

}
