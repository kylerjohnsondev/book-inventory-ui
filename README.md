# Book Inventory UI
The purpose of this project is to provide an example of reactive architecture. 

In this project you'll find:
- State management using services
- Angular Material table with client-side pagination and filtering
- Container/Presentation component architecture w/ OnPush change detection strategy
- Domain driven project structure
- shared and core modules
- Attribute directive

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.20.

## Requirements
- Angular/Angular CLI versions 8.3 or later
- Node version 10.9.0 or later

## To run locally
 
In your terminal, run:
- `git clone https://gitlab.com/kylerjohnsondev/book-inventory-ui.git`
- `npm i`
- `ng serve`

Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
